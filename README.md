# **Como configurar la variable de entorno JAVA_HOME y ANDROID_HOME en MacOS**

## **JAVA_HOME**

Antes que nada debemos tener instalado el JRE y el JDK. Para verificar la version instalada utilizar los siguientes comandos:
```
-   $ java -version
-   $ javac -version
```
![Verificacion de JRE y el JDK](./imagenes-java/comandos_java.png)

En caso de no estar instalado, descargarlo del [siguiente enlace](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html).

### ***A continuación***

-   Iremos a la carpeta 
```
$ cd ~/ 
```
-   Verificaremos la versión de java instalado.

/usr/libexec/java_home

![version](./imagenes-java/comando_version.png)

-   Importaremos el archivo.
```
$ vim .bash_profile 
```
![version](./imagenes-java/comando_vim.png)

-   Agregaremos las siguientes líneas dentro del archivo.
```
export JAVA_HOME=$(/usr/libexec/java_home)
```
![version](./imagenes-java/export_JAVA_HOME.png)


-   Para salir del archivo lo haremos presionando esc y después :wq

-   Al regresar al terminal, ingresaremos el siguiente comando.
```
$ source .bash_profile  
```
![version](./imagenes-java/comando_source.png)

- Para comprobar que se haya configurado, ingresar el comando.
```
$ echo $JAVA_HOME
```
![version](./imagenes-java/comando_echo.png)

## **ANDROID_HOME**

Previamente se debe tener instalado Android Studio

### ***Para configurar, seguiremos algunos de los pasos anteriores***

- Abriremos el archivo, con el siguiente comando.

```
$ vim .bash_profile
```

![version](./imagenes-android/comando_vim.png)

- Dentro del archivo, lo modificaremos y pegaremos las siguientes líneas.
```
export ANDROID_HOME=/Users/jonathan/Library/Android/sdk
export PATH=$PATH:$ANDROID_HOME/tools
export PATH=$PATH:$ANDROID_HOME/tools/bin
export PATH=$PATH/:$ANDROID_HOME/platform-tools
```

![version](./imagenes-android/comando_ANDROID_HOME.png)

-   Probaremos la configuración.
```
$ source .bash_profile
$ echo $ANDROID_HOME
```
![version](./imagenes-android/comando_android.png)


<a href="https://www.facebook.com/jonathan.parra.56" target="_blank"><img alt="Sígueme en Twitter" height="35" width="35" src="https://sguru.org/wp-content/uploads/2018/02/Facebook-PNG-Image-71244.png" title="Sígueme en Twitter"/> Jonathan Parra </a><br>
<a href="https://www.linkedin.com/in/jonathan-parra-a89a68162/" target="_blank"><img alt="Sígueme en LinkedIn" height="35" width="35" src="https://4.bp.blogspot.com/-0KtSvK3BydE/XCrIzgI3RqI/AAAAAAAAH_w/n_rr5DS92uk9EWEegcxeqAcSkV36OWEOgCLcBGAs/s1600/linkedin.png" title="Sígueme en LinkedIn"/> Jonathan Parra</a><br>
<a href="https://www.instagram.com/Choco_20jp/" target="_blank"><img alt="Sígueme en Instagram" height="35" width="35" src="https://4.bp.blogspot.com/-Ilxti1UuUuI/XCrIy6hBAcI/AAAAAAAAH_k/QV5KbuB9p3QB064J08W2v-YRiuslTZnLgCLcBGAs/s1600/instagram.png" title="Sígueme en Instagram"/> Choco_20jp</a><br>